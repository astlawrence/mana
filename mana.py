#!/usr/bin/env python3

"""Usage:
  mana.py [-q] [-s|-f] [-u] IP

Options:
  -h --help         Show this screen.
  --version         Show version.
"""

#pip3 install docopt

from subprocess import call, DEVNULL
from docopt import docopt
import threading

def status(mode, ip, ret):	
	if ret == 0:
		print("[+] " + ip + " scan completed: " + mode + ip)
	else:
		print("[-] " + ip + " scan failed to complete.")


# Change port option to all ports
def quickScanner(ip):
	cmd = "sudo nmap -sS -T5 -Pn -n -p- --reason -oN T_" + ip + " " + ip
	ret = call(cmd, shell=True, stderr=DEVNULL, stdout=DEVNULL)
	status("T_", ip, ret)
	

def deepScanner(mode, ip):	
	if mode is "f":
		cmd = "sudo nmap -sVT -A -T4 -Pn -n -p- --reason -oN TF_" + ip + " " + ip
		ret = call(cmd, shell=True, stderr=DEVNULL, stdout=DEVNULL)
		status("TF_", ip, ret)
	else:
		ports = ""
		fn = "T_" + ip
		
		with open(fn, "r") as f:
			lines = f.readlines()
			
			for line in lines:
				if line[0].isdigit():
					ports += line.split("/")[0] + ","
			
			ports = ports[:-1]
					
		cmd = "sudo nmap -sVT -A -T4 -Pn -n -p" + ports + " --reason -oN TS_" + ip + " " + ip
		ret = call(cmd, shell=True, stderr=DEVNULL, stdout=DEVNULL)
		status("TS_", ip, ret)

def udpScanner(ip):
	cmd = "sudo nmap -sUV -T5 -Pn -n -A --reason -oN U_" + ip + " " + ip
	ret = call(cmd, shell=True, stderr=DEVNULL, stdout=DEVNULL)
	status("U_", ip, ret)
	
if __name__ == '__main__':
	threads = []
	ip = []

	arguments = docopt(__doc__, version='mana 0.1')
	
	# Read IP addresses
	try:
		with open(arguments['IP'], mode="r") as f:
			ip = f.readlines()
	except:
		ip.append(arguments['IP'])
	
	if arguments['-q']:
		# TCP SYN scans all ports
		for i in ip:
			t = threading.Thread(target=quickScanner, args=(i.rstrip(),))
			threads.append(t)
			t.start()
			
		for t in threads:
			t.join()
		
		print("\n\u001b[32m[+] TCP SYN scans finished!\n\u001b[0m")	
			
	if arguments['-s'] and not arguments['-f']:
		# TCP version scans on selected ports reported by SYN scans
		for i in ip:
			t = threading.Thread(target=deepScanner, args=("s", i.rstrip(),))
			threads.append(t)
			t.start()
		
		for t in threads:
			t.join()
	
		print("\n\u001b[32m[+] TCP version scans on selected ports finished!\n\u001b[0m")	
		
	if arguments['-f'] and not arguments['-s']:
		# TCP version scans on all ports
		for i in ip:
			t = threading.Thread(target=deepScanner, args=("f", i.rstrip(),))
			threads.append(t)
			t.start()
		
		for t in threads:
			t.join()
	
		print("\n\u001b[32m[+] TCP version scans on all ports finished!\n\u001b[0m")	

	if arguments['-u']:
		# UDP scans on common ports
		for i in ip:
			t = threading.Thread(target=udpScanner, args=(i.rstrip(),))
			threads.append(t)
			t.start()
		
		for t in threads:
			t.join()
	
		print("\n\u001b[32m[+] UDP scans on common ports finished!\n\u001b[0m")	
